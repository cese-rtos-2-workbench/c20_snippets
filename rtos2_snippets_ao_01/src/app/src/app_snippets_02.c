/*
 * Copyright (c) 2023 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : app.c
 * @date   : Feb 17, 2023
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driver.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void task_button(void *argument)
{
  while (true)
  {
    state = button_read();

    led_event_t led_event = process_button_state(state);

    ao_led_send(&ao_led, led_event);

    vTaskDelay((TickType_t)((1000) / portTICK_PERIOD_MS));
  }
}

typedef enum
{
  LED_ON, LED_OFF,
} led_event_t;

void ao_led_send(ao_led_t *p_ao_led, led_event_t event)
{
  return (pdPASS == xQueueSend(p_ao_led->h_queue, (void*)&event, 0));
}

void task_ao_led(void *argument)
{
  ao_led_t *p_ao_led = (ao_led_t*)argument;
  while (true)
  {
    if(pdPASS == xQueueReceive(p_ao_led->h_queue, &event, portMAX_DELAY))
    {
      switch (event)
      {
        case LED_ON:
          hal_gpio_write(LED_RED, true);
          break;
        default:
          break;
      }
    }
  }
}

typedef struct
{
    QueueHandle_t h_queue;
} ao_led_t;

void ao_led_init(ao_led_t *p_ao_led)
{
  BaseType_t status;
  status = xTaskCreate(task_ao_led, "task_ao_led", 128, p_ao_led, tskIDLE_PRIORITY, NULL);
  while (pdPASS != status)
  {
    // error
  }

  p_ao_led->h_queue = xQueueCreate(QUEUE_LENGTH_, QUEUE_ITEM_SIZE_);
  while(NULL == p_ao_led->h_queue)
  {
    // error
  }
}

/********************** external functions definition ************************/

void app_init(void)
{
  // drivers
  {
    eboard_init();
  }

  ao_led_t ao_led;
  ao_led_init(&ao_led);

  // tasks
  {
    BaseType_t status;
    status = xTaskCreate(task_button, "task_button", 128, NULL, tskIDLE_PRIORITY, NULL);
    while (pdPASS != status)
    {
      // error
    }
  }
}

/********************** end of file ******************************************/
